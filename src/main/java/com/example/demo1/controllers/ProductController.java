package com.example.demo1.controllers;


import com.example.demo1.Demo1Application;
import com.example.demo1.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {


    static final String APIBaseUrl="/apitechu/v1";



    //CRUD
    @GetMapping(APIBaseUrl+"/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");


        return Demo1Application.productModels;
    }

    @GetMapping(APIBaseUrl+"/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("La id del producto a obtener es"+id);

        ProductModel result = new ProductModel();

        for (ProductModel product: Demo1Application.productModels) {
            if (product.getId().equals(id)){
                System.out.println("Producto encontrado");
                result=product;
            }
        }

        return result;
    }

    @PostMapping(APIBaseUrl+"/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("CreateProduct");

        Demo1Application.productModels.add(newProduct);


        return newProduct;
    }

    @PutMapping(APIBaseUrl+"/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");

        for (ProductModel productV: Demo1Application.productModels) {
            if (productV.getId().equals(id)){
                productV.setDesc(product.getDesc());
                productV.setPrice(product.getPrice());
            }
        }

        return product;
    }

    @PatchMapping(APIBaseUrl+"/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel product, @PathVariable String id){


        for (ProductModel productV: Demo1Application.productModels) {
            if (productV.getId().equals(id)){

                if (product.getDesc() != null){
                    productV.setDesc(product.getDesc());
                }
                if (product.getPrice()> 0){
                    productV.setPrice(product.getPrice());
                }
            }
        }

        return product;
    }

    @DeleteMapping(APIBaseUrl+"/products/{id}")
    public void deleteProduct(@PathVariable String id){

        for (ProductModel product: Demo1Application.productModels) {
            if (product.getId().equals(id)){
                Demo1Application.productModels.remove(product);
            }
        }

    }


}
