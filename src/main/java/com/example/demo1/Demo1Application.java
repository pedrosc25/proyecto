package com.example.demo1;

import com.example.demo1.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;


@SpringBootApplication
public class Demo1Application {

	public static ArrayList <ProductModel> productModels=new ArrayList<>();

	public static void main(String[] args) {

		SpringApplication.run(Demo1Application.class, args);

		Demo1Application.productModels = Demo1Application.getTestData();

	}
	private static ArrayList <ProductModel> getTestData(){


		productModels.add(new ProductModel("1","Producto 1",10));
		productModels.add(new ProductModel("2","Producto 2",20));
		productModels.add(new ProductModel("3","Producto 3",30));

		return productModels;
	}






}
